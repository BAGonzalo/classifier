# visuals
import numpy as np
import cv2
import torch
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import itertools
import seaborn as sns

def plot_ims(data, alpha=True, cmap=None, ncols=8, nrows=1): # plot_images(12)
    """
    data: expects (1) list of numpy array(s), (2) Variable(s) or (3) Tensor(s)
    """
    if type(data) == np.ndarray: print ("\
    data: expects (1) list of numpy array(s), (2) Variable(s) or (3) Tensor(s)"\
     )

    ncols = min(len(data), ncols)
    nrows = len(data) // ncols
    if len(data) % ncols != 0: nrows += 1

    fig, axes = plt.subplots(nrows=nrows, ncols=ncols, squeeze=False)
    fig.set_size_inches(3*ncols, 3*nrows)
    fig.tight_layout()

    for _, ax in enumerate(axes.flat):
        if _ < len(data):
            im = data[_]

            if isinstance(im, torch.autograd.Variable): im = im.data
            if isinstance(im, torch.Tensor):
                if len(im.shape) == 2: im = im.unsqueeze(0)
#                 im = np.reshape(im.numpy(), (im.shape[-2], im.shape[-1], im.shape[-3]))
                im = np.transpose(im.numpy(), (1, 2, 0))
#                 im = (im.transpose(-3, -1)).numpy()
            if len(im.shape) > 2:
                if (im.shape[2] == 4) and alpha: im = im[:,:,[2,1,0,3]]
                if (im.shape[2] == 3): im = im[:,:,[2,1,0]]

            im = im.squeeze()
            ax.imshow(im.astype(np.uint8), cmap=cmap)

        ax.set_axis_off()
        ax.title.set_visible(False)
        ax.xaxis.set_ticks([])
        ax.yaxis.set_ticks([])
        for spine in ax.spines.values(): spine.set_visible(False)

    plt.subplots_adjust(left=0, hspace=0, wspace=0)
    plt.tight_layout()
    plt.show()

def plot_cm(cm):   

    plt.figure(figsize=(10,10))
    ax = plt.subplot()
    sns.heatmap(cm.T, annot=True, ax=ax, linewidths=0, square=0, cmap=sns.cm.rocket); # cmap=sns.cm.rocket_r

    # labels, title and ticks
    ax.set_ylabel('Predicted labels');ax.set_xlabel('True labels'); 
    ax.set_title('Confusion Matrix') 
    # ax.xaxis.set_ticklabels(str(range(n_classes))) 
    # ax.yaxis.set_ticklabels(str(range(n_classes)))
    plt.savefig('confusion_matrix.jpg')
    plt.show()
    
def plot_results(per_epoch_log, n_classes, cm=True, epoch=0):

    # pre-process results     
    if per_epoch_log:
        batches = per_epoch_log[epoch]
        preds, labs = [], []
        for batch in batches:
            preds.append(list(batch[1].max(dim=1)[1].numpy()))
            labs.append(list(batch[2].numpy()))

    preds = np.asarray(list(itertools.chain.from_iterable(preds)))
    labs = np.asarray(list(itertools.chain.from_iterable(labs)))

    assert len(preds) == len(labs)
    TP = np.sum(preds == labs)
    TN = len(labs) - TP
    print('TP: %d | acc.: %.4f' %(TP, TP / len(labs)))
    res = np.vstack([labs, preds])

    # confusion matrix     
    per_class_preds = []
    for i in range(n_classes):
        class_preds = preds[np.where(labs == i)]
        per_class_preds.append(np.vstack([len(class_preds[class_preds == j]) for j in range(n_classes)]))

    c_m = np.hstack(per_class_preds)

    if cm == True: plot_cm(c_m)