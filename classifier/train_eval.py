import numpy as np
import torch
import torch.nn as nn

def check_shape(batch):
    if len(batch.shape) > 3 and batch.shape[1] not in [3, 4]:
        batch = batch.squeeze().unsqueeze(1)
    return batch

def train_and_eval_model(device, dataloaders, model, optimizer, loss_fn=None, scheduler=None, epochs=10, verbose=True, max_bs=32):

    modes = ['train', 'eval']
    losses = {i: [] for i in modes}
    ims_log = {i: [] for i in range(epochs)} # {epoch: (im, pred, label)}
    emb_log = {i: [] for i in range(epochs)} # {epoch: (im, pred, label)}
    
    print('\nTraining...')
    for e in range(epochs):
        for mode in modes:
            if mode == 'train': model.train()
            elif mode == 'eval': model.eval()
            else: print('Set "mode" param. to train or eval')

            per_batch_losses = []
            for _, batch in enumerate(dataloaders[mode]): 
                
                ims, labels = batch
                
                ims = ims.to(device)
                preds = model(ims)

                if isinstance (loss_fn, torch.nn.CrossEntropyLoss): 
                    labels = labels.max(dim=1)[1]
                loss = loss_fn(preds, labels)
                
                per_batch_losses.append(loss.item())
                
                ims_log[e].append((ims, labels)) # ((ims, pred_labels, labels))

                # backpropagate & update weights
                if mode == 'train': 
                    optimizer.zero_grad() 
                    # PyTorch accumulates gradients, which is very handy when you don't have enough 
                    # resources to calculate all the gradients you need in one go.
                    loss.backward()
                    optimizer.step()
                    optimizer.zero_grad() 
                    
            epoch_loss = np.asarray(per_batch_losses).mean()
            losses[mode].append(epoch_loss)
            
            if verbose:
                if mode == 'train': print('epoch: %d | training loss: %.10f | ' %(e, epoch_loss), end='')
                else: print('validation loss: %.10f' %epoch_loss)
                
        if scheduler: scheduler.step()
                
    return ims_log, emb_log, losses
    
