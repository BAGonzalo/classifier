# Template

PyTorch implementation of [ToyClassifier](https://arxiv.org/abs/1807.06653) propose architecture.

## Summary

ToyClassifier - NOTE: the data should be placed in the 'data' folder with the following structure:

-- data
	-- oven
		-- Apfelkuchen_-_fein,_Apple_sponge
			[images]

## Setting up

To run the code and notebooks, use the provided dockerfile images under classifier/dockerfiles/ [CPU-only support], as follows:

[1] Install [Docker Engine](https://www.docker.com/community-edition#/download); and clone the repository.

[2] Build the image, and run the environment by:

```
cd {repo-name}
docker build -t cpu -f dockerfiles/CPU .
docker run -it -v {global-path-to-repo}/{repo-name}:/{repo-name} -p 9000:9000 --user root --shm-size 16G --name test cpu
cd classifier
jupyter notebook --ip 0.0.0.0 --no-browser --port 9000 --allow-root 

```

To enter the running container from another terminal, run:

```
docker exec -it test bin/bash
```

[3] Access the Jupyter notebook tree at http://localhost:9000/, and enter the first token provided by the console -e.g.:

```
[I 13:30:53.425 NotebookApp] The Jupyter Notebook is running at:
[I 13:30:53.426 NotebookApp] http://809b15d36bae:9000/?token=5b849c9e10fc2671579c113e270f3953ad33113120ce28d6
```

The repository will be visualised as a files tree in your browser.

[4] Open and run the notebook/s.

[5] On run in the terminal.

```
python3.6 main.py  
```

## Authors

* **BAGonzalo** - https://gitlab.com/BAGonzalo (bagonzalo@gmail.com)